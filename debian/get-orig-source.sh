#!/bin/sh
# Get the DFSG-compatible source tarball from GitHub
# Author: 2014 Dmitry Shachnev

set -e

BRANCH="master"
MJ_COMMITS="https://github.com/mathjax/mathjax-docs/commits/$BRANCH"
MJ_TARBALL="https://github.com/mathjax/mathjax-docs/archive/$BRANCH.tar.gz"
DATE=`wget "$MJ_COMMITS" -qO- | grep -P '\d{4}-\d\d-\d\d' -o | sed 's/-//g' | head -n1`

pwd=$(pwd)

cd "$(dirname "$0")/../"
tmpdir=$(mktemp -t -d get-orig-source.XXXXXX)
cd "$tmpdir"

wget -nv "$MJ_TARBALL"
tar xzf "$BRANCH.tar.gz"
rm "$BRANCH.tar.gz"

BASEVERSION=`grep ^version "MathJax-docs-$BRANCH/conf.py" | grep '[0-9.]*' -o`
VERSION="$BASEVERSION+$DATE"
FILENAME="mathjax-docs_$VERSION.orig.tar.gz"

mv "MathJax-docs-$BRANCH" "mathjax-docs-$VERSION"
tar --sort=name --owner=root --group=root --mode=u=rwX,go=rX -I 'gzip -n' -cf "$pwd/$FILENAME" "mathjax-docs-$VERSION"
cd ..
rm -Rf "$tmpdir"

echo "Successfully created upstream tarball as $FILENAME."
